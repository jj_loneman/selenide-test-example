package com.example.dummy.unrollannotation

import com.example.dummy.debug.SpockDebugger
import com.example.pages.FormPage
import com.example.pages.SubmittedFormDataPage
import spock.lang.Specification
import spock.lang.Unroll

import static com.codeborne.selenide.Condition.text

class W3SchoolsFormPageObjectUnrollSpec extends Specification {

    def setupSpec() {
        SpockDebugger.initialize()
    }

    @Unroll
    def 'Test form submission is successful on W3Schools'(String firstName, String lastName) {
        given:
            def formPage = new FormPage()

        when: 'Name info is submitted into the form'
            formPage.setFirstNameField(firstName)
            formPage.setLastNameField(lastName)

        and: 'Switch to newly opened tab'
            def submittedFormDataPage = new SubmittedFormDataPage()
            def expectedData = text("firstname=${firstName}&lastname=${lastName}")

        then: 'New page should display the passed-in request params'
            submittedFormDataPage.getRequestParamsDiv().shouldHave(expectedData)

        where:
            firstName   | lastName
            'Clark'     | 'Kent'
            'Bruce'     | 'Wayne'
    }
}
