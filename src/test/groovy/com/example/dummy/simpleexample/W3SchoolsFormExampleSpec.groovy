package com.example.dummy.simpleexample

import com.example.dummy.debug.SpockDebugger
import spock.lang.Specification

import static com.codeborne.selenide.Condition.text
import static com.codeborne.selenide.Selectors.byName
import static com.codeborne.selenide.Selenide.*

class W3SchoolsFormExampleSpec extends Specification {

    def setupSpec() {
        SpockDebugger.initialize()
    }

    def 'Test form submission is successful on W3Schools'() {
        when: 'Name info is submitted into the form'
            open('https://www.w3schools.com/html/html_forms.asp')

            $(byName('firstname')).setValue('Clark')
            $(byName('lastname')).setValue('Kent')

            $x('//*[@id="main"]/div[3]/div/form').submit()

        and: 'Switch to newly opened tab'
            switchTo().window(1)

        then: 'New page should display the passed-in request params'
            $x('/html/body/div[1]').shouldHave(text('firstname=Clark&lastname=Kent'))
    }
}
