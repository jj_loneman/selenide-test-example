package com.example.dummy.debug

import com.codeborne.selenide.Configuration

import java.nio.file.Paths


class SpockDebugger {

    /**
     * If debugging, change this to whatever browser you want to test in: either 'chrome' or 'firefox'
     */
    static final testBrowser = 'chrome'

    static void initialize() {

        /*
         * This will be true if we execute a spec from the IDE (not using Gradle).
         * This will be false if we execute the spec from Gradle.
         */
        final isDebug = !System.properties['org.gradle.native']

        if (isDebug) {
            final seleniumDriverDir = Paths.get("./seleniumDrivers").toAbsolutePath().normalize()

            def testBrowsers = [
                    'chrome' : 'chromedriver.exe',
                    'firefox': 'geckodriver.exe',
            ]

            final testBrowserDriver = testBrowsers[testBrowser]
            final webDriverKey = "webdriver.${testBrowser}.driver" as String
            final webDriverValue = "$seleniumDriverDir/$testBrowserDriver" as String

            System.properties[webDriverKey] = webDriverValue
            Configuration.browser = testBrowser
        }
    }
}
