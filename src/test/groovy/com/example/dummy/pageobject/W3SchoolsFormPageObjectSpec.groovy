package com.example.dummy.pageobject

import com.example.dummy.debug.SpockDebugger
import com.example.pages.FormPage
import com.example.pages.SubmittedFormDataPage
import spock.lang.Specification

import static com.codeborne.selenide.Condition.text

class W3SchoolsFormPageObjectSpec extends Specification {

    def setupSpec() {
        SpockDebugger.initialize()
    }

    def 'Test "Clark Kent" form submission is successful on W3Schools'() {
        given:
            def formPage = new FormPage()

        when: 'Name info is submitted into the form'
            formPage.setFirstNameField('Clark')
            formPage.setLastNameField('Kent')

        and: 'Switch to newly opened tab'
            def submittedFormDataPage = new SubmittedFormDataPage()
            def expectedData = text('firstname=Clark&lastname=Kent')

        then: 'New page should display the passed-in request params'
            submittedFormDataPage.getRequestParamsDiv().shouldHave(expectedData)
    }

    def 'Test "Bruce Wayne" form submission is successful on W3Schools'() {
        given:
            def formPage = new FormPage()

        when: 'Name info is submitted into the form'
            formPage.setFirstNameField('Bruce')
            formPage.setLastNameField('Wayne')

        and: 'Switch to newly opened tab'
            def submittedFormDataPage = new SubmittedFormDataPage()
            def expectedData = text('firstname=Bruce&lastname=Wayne')

        then: 'New page should display the passed-in request params'
            submittedFormDataPage.getRequestParamsDiv().shouldHave(expectedData)
    }
}
