package com.example.dummy.simpleexample;

import org.junit.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$x;

public class W3SchoolsFormExampleTest {

    @Test
    public void W3SchoolsForm_ValidNameValues_FormSubmissionIsSuccess() {
        open("https://www.w3schools.com/html/html_forms.asp");

        $(byName("firstname")).setValue("Clark");
        $(byName("lastname")).setValue("Kent");

        $x("//*[@id=\"main\"]/div[3]/div/form").submit();

        // Switch to newly opened tab
        switchTo().window(1);

        $x("/html/body/div[1]").shouldHave(text("firstname=Clark&lastname=Kent"));
    }
}
