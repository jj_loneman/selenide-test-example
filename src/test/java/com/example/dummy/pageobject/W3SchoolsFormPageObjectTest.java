package com.example.dummy.pageobject;

import com.codeborne.selenide.Condition;
import com.example.pages.FormPage;
import com.example.pages.SubmittedFormDataPage;
import org.junit.Test;

import static com.codeborne.selenide.Condition.text;

public class W3SchoolsFormPageObjectTest {

    @Test
    public void W3SchoolsForm_ClarkKentAsValidNameValue_FormSubmissionIsSuccess() {
        FormPage formPage = new FormPage();
        formPage.setFirstNameField("Clark");
        formPage.setLastNameField("Kent");

        SubmittedFormDataPage submittedFormDataPage = new SubmittedFormDataPage();
        Condition expectedData = text("firstname=Clark&lastname=Kent");

        submittedFormDataPage.getRequestParamsDiv().shouldHave(expectedData);
    }

    @Test
    public void W3SchoolsForm_BruceWayneAsValidNameValue_FormSubmissionIsSuccess() {
        FormPage formPage = new FormPage();
        formPage.setFirstNameField("Bruce");
        formPage.setLastNameField("Wayne");

        SubmittedFormDataPage submittedFormDataPage = new SubmittedFormDataPage();
        Condition expectedData = text("firstname=Bruce&lastname=Wayne");

        submittedFormDataPage.getRequestParamsDiv().shouldHave(expectedData);
    }

}
