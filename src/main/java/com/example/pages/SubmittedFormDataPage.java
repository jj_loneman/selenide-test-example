package com.example.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.switchTo;

public class SubmittedFormDataPage {

    private SelenideElement requestParamsDiv;

    public SubmittedFormDataPage() {
        switchTo().window(1);
        requestParamsDiv = $x("/html/body/div[1]");
    }

    public SelenideElement getRequestParamsDiv() {
        return requestParamsDiv;
    }
}
