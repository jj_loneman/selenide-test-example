package com.example.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class FormPage {

    private static final String url = "https://www.w3schools.com/html/html_forms.asp";
    private SelenideElement firstNameField;
    private SelenideElement lastNameField;
    private SelenideElement form;

    public FormPage() {
        open(url);

        firstNameField = $(byName("firstname"));
        lastNameField = $(byName("lastname"));
        form = $x("//*[@id=\"main\"]/div[3]/div/form");
    }

    public void setFirstNameField(String firstName) {
        this.firstNameField.setValue(firstName);
    }

    public void setLastNameField(String lastName) {
        this.lastNameField.setValue(lastName);
    }

    public void submitForm() {
        this.form.submit();
    }
}
