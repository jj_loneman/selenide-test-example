# Selenide Test Example

> Selenide is a Selenium wrapper.
> This project contains examples in both Spock (Groovy) and JUnit (Java).

## Cloning the Project

```commandline
git clone https://bitbucket.org/jj_loneman/selenide-test-example.git
cd selenide-test-example
```

## Specifying the Tests to Run

In `build.gradle` on line 106, there is a `filter` closure where you can
specify which tests to run:

```groovy
// Change this filter to whatever tests you want to include
filter {
    include 'com/example/dummy/simpleexample/W3SchoolsFormExampleSpec.class'
}
```

If you want to run all tests in the package `com.example.dummy`, for example,
you can change the argument in `include` to:

```groovy
    include 'com/example/dummy/*'
```

Then, run the following Gradle task:

```commandline
gradlew check
``` 
    
## Selenide Documentation

* [Selenide Github - Most Common Methods](https://github.com/codeborne/selenide/wiki/Selenide-vs-Selenium)
* [Selenide Github - Main Page](https://github.com/codeborne/selenide)
* [Selenide Github - Quick Start](https://github.com/codeborne/selenide/wiki/Quick-Start)
* [Selenide Javadoc](http://selenide.org/javadoc.html)
    